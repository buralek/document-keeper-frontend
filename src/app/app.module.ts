import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TestComponent} from './component/test/test.component';
import {AppRoutingModule} from './app-routing.module';
import {CookieService} from 'ngx-cookie-service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './component/authorization/login/login.component';
import {HeaderComponent} from './component/header/header.component';
import {HttpErrorInterceptor} from './interceptor/http-error.interceptor';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthAccessComponent} from './component/authorization/auth-access/auth-access.component';
import {AuthTokenInterceptor} from './interceptor/auth-token.interceptor';
import {MainPageComponent} from './component/main-page/main-page.component';
import {MatTabsModule} from '@angular/material/tabs';
import {SalesCheckFilterComponent} from './component/sales-check/sales-check-filter/sales-check-filter.component';
import {DateFieldComponent} from './component/shared/date-field/date-field.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {SalesCheckCardComponent} from './component/sales-check/sales-check-card/sales-check-card.component';
import {SalesCheckListComponent} from './component/sales-check/sales-check-list/sales-check-list.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDividerModule} from '@angular/material/divider';
import {ChangeCompanyDialogComponent} from './component/sales-check/sales-check-card/change-company-dialog/change-company-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {CreateSalesCheckComponent} from './component/sales-check/create-sales-check/create-sales-check.component';
import { CreateCompanyDialogComponent } from './component/sales-check/create-sales-check/create-company-dialog/create-company-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    LoginComponent,
    HeaderComponent,
    AuthAccessComponent,
    MainPageComponent,
    SalesCheckFilterComponent,
    DateFieldComponent,
    SalesCheckCardComponent,
    SalesCheckListComponent,
    ChangeCompanyDialogComponent,
    CreateSalesCheckComponent,
    CreateCompanyDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatExpansionModule,
    MatGridListModule,
    MatDividerModule,
    MatDialogModule
  ],
  providers: [
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
