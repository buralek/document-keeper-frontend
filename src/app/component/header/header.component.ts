import { Component, OnInit } from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {USER_NAME} from '../../const/auth-service-const';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username: string;
  constructor(private cookieService: CookieService) { }

  ngOnInit(): void {
    this.username = this.cookieService.get(USER_NAME);
  }

  accountIcon() {

  }

}
