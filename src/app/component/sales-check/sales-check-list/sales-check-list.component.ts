import {Component, OnInit} from '@angular/core';
import {StorageService} from '../../../service/storage/storage.service';
import {SalesCheckService} from '../../../service/sales-check-service/sales-check/sales-check.service';
import {SalesCheckDto} from '../../../service/sales-check-service/model/sales-check-dto';
import {GetSalesChecksByIdsRequest} from '../../../service/sales-check-service/sales-check/model/get-sales-checks-by-ids-request';
import {GetSalesChecksByIdsResponse} from '../../../service/sales-check-service/sales-check/model/get-sales-checks-by-ids-response';
import {CookieService} from 'ngx-cookie-service';
import {USER_NAME} from '../../../const/auth-service-const';
import {Router} from '@angular/router';
import {CompanyService} from '../../../service/sales-check-service/company/company.service';
import {CompanyDto} from '../../../service/sales-check-service/model/company-dto';

@Component({
  selector: 'app-sales-check-list',
  templateUrl: './sales-check-list.component.html',
  styleUrls: ['./sales-check-list.component.css']
})
export class SalesCheckListComponent implements OnInit {
  filteredSalesChecks: SalesCheckDto[];
  companies: CompanyDto[];
  constructor(private storageService: StorageService,
              private salesCheckService: SalesCheckService,
              private companyService: CompanyService,
              private cookieService: CookieService,
              private router: Router ) {
  }

  ngOnInit(): void {
    this.storageService.filteredSalesCheckIds$.subscribe(filteredSalesCheckIds => {
      const request = new GetSalesChecksByIdsRequest();
      request.salesCheckIds = filteredSalesCheckIds;
      request.userName = this.cookieService.get(USER_NAME);
      this.salesCheckService.getSalesChecksByIds(request).subscribe((response: GetSalesChecksByIdsResponse) => {
        this.filteredSalesChecks = response.salesChecks;
      });
    });
    this.companyService.getAllCompanies().subscribe(response => {
      this.companies = response.companies;
    });
  }

  createSalesCheck() {
    this.router.navigate(['create-sales-check']);
  }

}


