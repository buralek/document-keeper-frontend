import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesCheckListComponent } from './sales-check-list.component';

describe('SalesCheckListComponent', () => {
  let component: SalesCheckListComponent;
  let fixture: ComponentFixture<SalesCheckListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCheckListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCheckListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
