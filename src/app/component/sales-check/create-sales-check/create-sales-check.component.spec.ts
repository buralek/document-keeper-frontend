import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSalesCheckComponent } from './create-sales-check.component';

describe('CreateSalesCheckComponent', () => {
  let component: CreateSalesCheckComponent;
  let fixture: ComponentFixture<CreateSalesCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSalesCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSalesCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
