import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CompanyService} from '../../../../service/sales-check-service/company/company.service';
import {AddCompanyRequest} from '../../../../service/sales-check-service/company/model/add-company-request';

@Component({
  selector: 'app-create-company-dialog',
  templateUrl: './create-company-dialog.component.html',
  styleUrls: ['./create-company-dialog.component.css']
})
export class CreateCompanyDialogComponent implements OnInit {
  companyFormGroup = new FormGroup({
    companyNameInput: new FormControl(),
    companyAddressInput: new FormControl(),
    companyPhoneInput: new FormControl()
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private companyService: CompanyService) { }

  ngOnInit(): void {
    this.companyFormGroup.get('companyNameInput').setValue(this.data.company.name);
    this.companyFormGroup.get('companyAddressInput').setValue(this.data.company.address);
    this.companyFormGroup.get('companyPhoneInput').setValue(this.data.company.phone);
  }

  create() {
    const request = new AddCompanyRequest();
    request.name = this.companyFormGroup.get('companyNameInput').value;
    request.address = this.companyFormGroup.get('companyAddressInput').value;
    request.phone = this.companyFormGroup.get('companyPhoneInput').value;
    this.companyService.addCompany(request).subscribe(response => {
      window.location.reload();
    });
  }

}
