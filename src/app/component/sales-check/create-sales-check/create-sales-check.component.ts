import {Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {SalesCheckService} from '../../../service/sales-check-service/sales-check/sales-check.service';
import {CompanyService} from '../../../service/sales-check-service/company/company.service';
import {Types} from '../../../const/types';
import {CompanyDto} from '../../../service/sales-check-service/model/company-dto';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AddSalesCheckRequest} from '../../../service/sales-check-service/sales-check/model/add-sales-check-request';
import {USER_NAME} from '../../../const/auth-service-const';
import {WarrantyDto} from '../../../service/sales-check-service/sales-check/model/warranty-dto';
import {ChangeCompanyDialogComponent} from '../sales-check-card/change-company-dialog/change-company-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {CreateCompanyDialogComponent} from './create-company-dialog/create-company-dialog.component';

@Component({
  selector: 'app-create-sales-check',
  templateUrl: './create-sales-check.component.html',
  styleUrls: ['./create-sales-check.component.css']
})
export class CreateSalesCheckComponent implements OnInit {
  createFormGroup = new FormGroup({
    nameInput: new FormControl('', [
      Validators.required
    ]),
    typeInput: new FormControl(),
    companyIdInput: new FormControl(),
    companyNameInput: new FormControl(),
    companyAddressInput: new FormControl(),
    companyPhoneInput: new FormControl(),
    purchaseDateInput: new FormControl(),
    warrantyFromDateInput: new FormControl(),
    warrantyToDateInput: new FormControl(),
  });

  types = Types;
  typeKeys = Object.keys(Types);

  companies: CompanyDto[];

  constructor(private cookieService: CookieService,
              private salesCheckService: SalesCheckService,
              private companyService: CompanyService,
              private router: Router,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.companyService.getAllCompanies().subscribe(response => {
      this.companies = response.companies;
    });
    this.createFormGroup.get('companyNameInput').disable();
    this.createFormGroup.get('companyAddressInput').disable();
    this.createFormGroup.get('companyPhoneInput').disable();
  }

  cancel() {
    this.router.navigate(['main-page']);
  }

  create() {
    const request = new AddSalesCheckRequest();
    request.userName = this.cookieService.get(USER_NAME);
    request.name = this.createFormGroup.get('nameInput').value;
    request.typeId = this.createFormGroup.get('typeInput').value;
    request.purchaseDate = this.createFormGroup.get('purchaseDateInput').value;
    request.companyId = this.createFormGroup.get('companyIdInput').value;

    const warrantyDto = new WarrantyDto();
    warrantyDto.fromDate = this.createFormGroup.get('warrantyFromDateInput').value;
    warrantyDto.toDate = this.createFormGroup.get('warrantyToDateInput').value;
    request.warrantyDto = warrantyDto;

    this.salesCheckService.addSalesCheck(request).subscribe(response => {
      console.log('New Sales Check has been created with id ' + response.addedSalesCheckId);
      this.router.navigate(['main-page']);
    });
  }

  onPurchaseDateChange(newDate: Date) {
    this.createFormGroup.get('purchaseDateInput').setValue(newDate);
  }

  onWarrantyFromDateChange(newDate: Date) {
    this.createFormGroup.get('warrantyFromDateInput').setValue(newDate);
  }

  onWarrantyToDateChange(newDate: Date) {
    this.createFormGroup.get('warrantyToDateInput').setValue(newDate);
  }

  createNewCompany() {
    this.dialog.open(CreateCompanyDialogComponent,
      {
        width: '650px'
      });
  }
}
