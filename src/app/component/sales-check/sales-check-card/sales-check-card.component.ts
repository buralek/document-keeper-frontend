import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Types} from '../../../const/types';
import {SalesCheckDto} from '../../../service/sales-check-service/model/sales-check-dto';
import {SalesCheckService} from '../../../service/sales-check-service/sales-check/sales-check.service';
import {UpdateSalesCheckRequest} from '../../../service/sales-check-service/sales-check/model/update-sales-check-request';
import {CookieService} from 'ngx-cookie-service';
import {USER_NAME} from '../../../const/auth-service-const';
import {UpdateSalesCheckDto} from '../../../service/sales-check-service/sales-check/model/update-sales-check-dto';
import {WarrantyDto} from '../../../service/sales-check-service/sales-check/model/warranty-dto';
import {CompanyDto} from '../../../service/sales-check-service/model/company-dto';
import {MatDialog} from '@angular/material/dialog';
import {ChangeCompanyDialogComponent} from './change-company-dialog/change-company-dialog.component';

@Component({
  selector: 'app-sales-check-card',
  templateUrl: './sales-check-card.component.html',
  styleUrls: ['./sales-check-card.component.css']
})
export class SalesCheckCardComponent implements OnInit {
  cardFormGroup = new FormGroup({
    idInput: new FormControl(),
    nameInput: new FormControl(),
    typeInput: new FormControl(),
    companyIdInput: new FormControl(),
    companyNameInput: new FormControl(),
    companyAddressInput: new FormControl(),
    companyPhoneInput: new FormControl(),
    purchaseDateInput: new FormControl(),
    warrantyFromDateInput: new FormControl(),
    warrantyToDateInput: new FormControl(),
  });

  panelOpenState = false;

  types = Types;
  typeKeys = Object.keys(Types);

  @Input() companies: CompanyDto[];

  @Input() salesCheck: SalesCheckDto;

  constructor(private salesCheckService: SalesCheckService,
              private cookieService: CookieService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.cardFormGroup.get('idInput').setValue(this.salesCheck.id);
    this.cardFormGroup.get('idInput').disable();
    this.cardFormGroup.get('nameInput').setValue(this.salesCheck.name);
    this.cardFormGroup.get('purchaseDateInput').setValue(this.salesCheck.purchaseDate);
    this.cardFormGroup.get('typeInput').setValue(this.salesCheck.type.id);

    this.cardFormGroup.get('companyIdInput').setValue(this.salesCheck.company.id);
    this.cardFormGroup.get('companyNameInput').setValue(this.salesCheck.company.name);
    this.cardFormGroup.get('companyAddressInput').setValue(this.salesCheck.company.address);
    this.cardFormGroup.get('companyPhoneInput').setValue(this.salesCheck.company.phone);
    this.cardFormGroup.get('companyNameInput').disable();
    this.cardFormGroup.get('companyAddressInput').disable();
    this.cardFormGroup.get('companyPhoneInput').disable();

    this.cardFormGroup.get('warrantyFromDateInput').setValue(this.salesCheck.warranty.fromDate);
    this.cardFormGroup.get('warrantyToDateInput').setValue(this.salesCheck.warranty.toDate);
  }

  onPurchaseDateChange(newDate: Date) {
    this.cardFormGroup.get('purchaseDateInput').setValue(newDate);
    const request = this.createUpdateSalesCheckRequest();
    request.updatePart.purchaseDate = newDate;
    this.salesCheckService.updateSalesCheck(request).subscribe(response => {
      this.salesCheck = response.salesCheck;
    });
  }

  onWarrantyFromDateChange(newDate: Date) {
    this.cardFormGroup.get('warrantyFromDateInput').setValue(newDate);
    const request = this.createUpdateSalesCheckRequest();
    const warrantyPart = new WarrantyDto();
    warrantyPart.fromDate = newDate;
    request.updatePart.warranty = warrantyPart;
    this.salesCheckService.updateSalesCheck(request).subscribe(response => {
      this.salesCheck = response.salesCheck;
    });
  }

  onWarrantyToDateChange(newDate: Date) {
    this.cardFormGroup.get('warrantyToDateInput').setValue(newDate);
    const request = this.createUpdateSalesCheckRequest();
    const warrantyPart = new WarrantyDto();
    warrantyPart.toDate = newDate;
    request.updatePart.warranty = warrantyPart;
    this.salesCheckService.updateSalesCheck(request).subscribe(response => {
      this.salesCheck = response.salesCheck;
    });
  }

  onNameInputChange(value) {
    const request = this.createUpdateSalesCheckRequest();
    request.updatePart.name = value;
    this.salesCheckService.updateSalesCheck(request).subscribe(response => {
      this.salesCheck = response.salesCheck;
    });
  }

  onCompanyIdInputChange(value) {
    const request = this.createUpdateSalesCheckRequest();
    request.updatePart.companyId = value;
    this.salesCheckService.updateSalesCheck(request).subscribe(response => {
      this.salesCheck = response.salesCheck;
      this.cardFormGroup.get('companyNameInput').setValue(this.salesCheck.company.name);
      this.cardFormGroup.get('companyAddressInput').setValue(this.salesCheck.company.address);
      this.cardFormGroup.get('companyPhoneInput').setValue(this.salesCheck.company.phone);
    });
  }

  onTypeInputChange(value) {
    const request = this.createUpdateSalesCheckRequest();
    request.updatePart.typeId = value;
    this.salesCheckService.updateSalesCheck(request).subscribe(response => {
      this.salesCheck = response.salesCheck;
    });
  }

  createUpdateSalesCheckRequest(): UpdateSalesCheckRequest {
    const request = new UpdateSalesCheckRequest();
    request.id = this.cardFormGroup.get('idInput').value;
    request.userName = this.cookieService.get(USER_NAME);
    request.updatePart =  new UpdateSalesCheckDto();
    return request;
  }

  changeCompanyFields() {
    this.dialog.open(ChangeCompanyDialogComponent,
      {
        width: '650px',
        data: {
          company: this.salesCheck.company
        }
      });
  }

  deleteSalesCheck() {
    this.salesCheckService.deleteSalesCheck(this.salesCheck.id).subscribe(() => {
      console.log('Sales check with id ' + this.salesCheck.id + ' has been deleted');
      window.location.reload();
    });
  }
}
