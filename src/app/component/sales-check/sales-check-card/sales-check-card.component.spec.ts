import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesCheckCardComponent } from './sales-check-card.component';

describe('SalesCheckCardComponent', () => {
  let component: SalesCheckCardComponent;
  let fixture: ComponentFixture<SalesCheckCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCheckCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCheckCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
