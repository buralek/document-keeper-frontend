import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup} from '@angular/forms';
import {CompanyService} from '../../../../service/sales-check-service/company/company.service';
import {UpdateCompanyRequest} from '../../../../service/sales-check-service/company/model/update-company-request';
import {UpdateCompanyPart} from '../../../../service/sales-check-service/company/model/update-company-part';

@Component({
  selector: 'app-change-company-dialog',
  templateUrl: './change-company-dialog.component.html',
  styleUrls: ['./change-company-dialog.component.css']
})
export class ChangeCompanyDialogComponent implements OnInit {
  companyFormGroup = new FormGroup({
    companyNameInput: new FormControl(),
    companyAddressInput: new FormControl(),
    companyPhoneInput: new FormControl()
  });

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private companyService: CompanyService) { }

  ngOnInit(): void {
    this.companyFormGroup.get('companyNameInput').setValue(this.data.company.name);
    this.companyFormGroup.get('companyAddressInput').setValue(this.data.company.address);
    this.companyFormGroup.get('companyPhoneInput').setValue(this.data.company.phone);
  }

  update() {
    const request = new UpdateCompanyRequest();
    const updatePart = new UpdateCompanyPart();
    updatePart.name = this.companyFormGroup.get('companyNameInput').value;
    updatePart.address = this.companyFormGroup.get('companyAddressInput').value;
    updatePart.phone = this.companyFormGroup.get('companyPhoneInput').value;
    request.updatePart = updatePart;
    request.id = this.data.company.id;
    this.companyService.updateCompany(request).subscribe(response => {
      window.location.reload();
    });
  }
}
