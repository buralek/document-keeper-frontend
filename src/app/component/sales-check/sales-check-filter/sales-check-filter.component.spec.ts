import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesCheckFilterComponent } from './sales-check-filter.component';

describe('SalesCheckFilterComponent', () => {
  let component: SalesCheckFilterComponent;
  let fixture: ComponentFixture<SalesCheckFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCheckFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCheckFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
