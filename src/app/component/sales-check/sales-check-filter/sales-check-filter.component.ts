import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {SalesCheckFilterService} from '../../../service/sales-check-service/filter/sales-check-filter.service';
import {FilteredSalesChecksRequest} from '../../../service/sales-check-service/filter/model/filtered-sales-checks-request';
import {Types} from '../../../const/types';
import {FilteredSalesChecksResponse} from '../../../service/sales-check-service/filter/model/filtered-sales-checks-response';
import {StorageService} from '../../../service/storage/storage.service';
import {CookieService} from 'ngx-cookie-service';
import {USER_NAME} from '../../../const/auth-service-const';

@Component({
  selector: 'app-sales-check-filter',
  templateUrl: './sales-check-filter.component.html',
  styleUrls: ['./sales-check-filter.component.css']
})
export class SalesCheckFilterComponent implements OnInit {
  filterFormGroup = new FormGroup({
    idInput: new FormControl(),
    nameInput: new FormControl(),
    typeInput: new FormControl(),
    companyInput: new FormControl(),
    dateFromInput: new FormControl(),
    dateToInput: new FormControl()
  });

  dateFromLabel = 'Choose a From date';
  dateToLabel = 'Choose a To date';

  types = Types;
  typeKeys = Object.keys(Types);

  filteredSalesCheckIds: string[];

  constructor(private salesCheckFilterService: SalesCheckFilterService,
              private  storageService: StorageService,
              private cookieService: CookieService) {
  }

  ngOnInit(): void {
    this.storageService.filteredSalesCheckIds$.subscribe(filteredSalesCheckIds => this.filteredSalesCheckIds = filteredSalesCheckIds);
    this.filter();
  }

  filter() {
    const filteredRequest: FilteredSalesChecksRequest  = new FilteredSalesChecksRequest();
    filteredRequest.userName = this.cookieService.get(USER_NAME);
    filteredRequest.id = this.filterFormGroup.get('idInput').value;
    filteredRequest.name = this.filterFormGroup.get('nameInput').value;
    filteredRequest.companyName = this.filterFormGroup.get('companyInput').value;
    filteredRequest.typeId = this.filterFormGroup.get('typeInput').value;
    filteredRequest.fromDate = this.filterFormGroup.get('dateFromInput').value;
    filteredRequest.toDate = this.filterFormGroup.get('dateToInput').value;
    this.deleteEmptyElements(filteredRequest);

    this.salesCheckFilterService.getFilteredSalesChecks(filteredRequest).subscribe((response: FilteredSalesChecksResponse) => {
      this.filteredSalesCheckIds = response.filteredSalesCheckIds;
      this.storageService.changeFilteredSalesCheckIds(this.filteredSalesCheckIds);
      console.log('Filtered sales checks response:' + JSON.stringify(response));
    });
  }

  addFromDate(newDate: Date) {
    this.filterFormGroup.get('dateFromInput').setValue(newDate);
  }

  addToDate(newDate: Date) {
    this.filterFormGroup.get('dateToInput').setValue(newDate);
  }

  deleteEmptyElements(obj) {
    for (let propName in obj) {
      if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
        delete obj[propName];
      }
    }
  }

}
