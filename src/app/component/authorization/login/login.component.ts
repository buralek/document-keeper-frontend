import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../service/auth-service/auth/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public oauthPage = '';

  loginFormGroup = new FormGroup({
    loginInput: new FormControl('', [
      Validators.required
    ]),
    passwordInput: new FormControl('', [
      Validators.required
    ])
  });

  constructor(public authService: AuthService,
              private cookieService: CookieService) { }

  ngOnInit(): void {
  }

  logout() {
    this.authService.deleteToken();
  }

  signIn() {
    this.authService.login(this.loginFormGroup.get('loginInput').value, this.loginFormGroup.get('passwordInput').value).subscribe(
      sessionId => {
        this.cookieService.delete('JSESSIONID');
        this.cookieService.set('JSESSIONID', sessionId.toString());
        this.authService.deleteToken();
        this.authService.getAuthCode().subscribe(response => {
          this.oauthPage = response;
          if (this.oauthPage !== '') {
            this.authService.setAuthenticated(true);
          }
        });
      }
    );
  }
}
