import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../service/auth-service/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth-access',
  templateUrl: './auth-access.component.html',
  styleUrls: ['./auth-access.component.css']
})
export class AuthAccessComponent implements OnInit {

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }

  authorize() {
    this.authService.confirmAuthorize().subscribe(token => {
      console.log('Token = ' + token);
      this.authService.saveAuthInfo(token);
    });
  }

  deny() {
    this.authService.setAuthenticated(false);
    this.router.navigate(['login']);
  }

}
