import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../service/account-service/account/account.service';
import {AllAccountsResponse} from '../../service/account-service/account/model/all-accounts-response';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  allAccountsResponse: AllAccountsResponse;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.accountService.findAllAccounts().subscribe((response: AllAccountsResponse) => {
      this.allAccountsResponse = response;
      console.log('All account response is : ' + this.allAccountsResponse);
    });
  }

}
