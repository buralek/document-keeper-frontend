import {ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';

@Component({
  selector: 'app-date-field',
  templateUrl: './date-field.component.html',
  styleUrls: ['./date-field.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateFieldComponent),
      multi: true
    }
  ]
})
export class DateFieldComponent implements ControlValueAccessor {
  dateForm = new FormGroup({
    dateInput: new FormControl()});

  @Input() dateLabel: string;
  @Output() newDateEvent = new EventEmitter<Date>();

  constructor() {
  }

  writeValue(value: any) {
    if (value !== undefined) {
      this.dateForm.get('dateInput').setValue(value);
    }
  }

  propagateChange = (_: any) => {};

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

  addNewDate(event: MatDatepickerInputEvent<Date>) {
    this.newDateEvent.emit(event.value);
  }
}
