import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {
  ADD_PATH,
  ALL_PATH,
  COMPANY_PATH,
  SALES_CHECK_PATH,
  SALES_CHECK_SERVICE_PATH,
  UPDATE_PATH
} from '../../../const/sales-check-service-const';
import {HttpClient} from '@angular/common/http';
import {AllCompaniesResponse} from './model/all-companies-response';
import {UpdateCompanyRequest} from './model/update-company-request';
import {UpdateCompanyResponse} from './model/update-company-response';
import {AddSalesCheckRequest} from '../sales-check/model/add-sales-check-request';
import {AddSalesCheckResponse} from '../sales-check/model/add-sales-check-response';
import {AddCompanyRequest} from './model/add-company-request';
import {AddCompanyResponse} from './model/add-company-response';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient) { }

  getAllCompanies(): Observable<AllCompaniesResponse> {
    return this.http.get<AllCompaniesResponse>(SALES_CHECK_SERVICE_PATH + COMPANY_PATH + ALL_PATH);
  }

  updateCompany(request: UpdateCompanyRequest): Observable<UpdateCompanyResponse> {
    return this.http.put<UpdateCompanyResponse>(SALES_CHECK_SERVICE_PATH + COMPANY_PATH + UPDATE_PATH, request);
  }

  addCompany(request: AddCompanyRequest): Observable<AddCompanyResponse> {
    return this.http.post<AddCompanyResponse>(SALES_CHECK_SERVICE_PATH + COMPANY_PATH + ADD_PATH, request);
  }
}
