import {CompanyDto} from '../../model/company-dto';

export class UpdateCompanyResponse {
  company: CompanyDto;
}
