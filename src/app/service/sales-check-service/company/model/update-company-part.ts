export class UpdateCompanyPart {
  name: string;
  address: string;
  phone: string;
}
