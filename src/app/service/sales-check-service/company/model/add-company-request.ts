export class AddCompanyRequest {
  name: string;
  address: string;
  phone: string;
}
