import {UpdateCompanyPart} from './update-company-part';

export class UpdateCompanyRequest {
  id: string;
  updatePart: UpdateCompanyPart;
}
