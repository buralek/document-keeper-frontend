import {CompanyDto} from '../../model/company-dto';

export class AllCompaniesResponse {
  companies: CompanyDto[];
}
