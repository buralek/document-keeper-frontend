import {TypeDto} from './type-dto';
import {WarrantyDto} from './warranty-dto';
import {CompanyDto} from './company-dto';

export class SalesCheckDto {
  id: string;
  name: string;
  userName: string;
  purchaseDate: Date;
  type: TypeDto;
  warranty: WarrantyDto;
  company: CompanyDto;
}
