export class CompanyDto {
  id: string;
  name: string;
  address: string;
  phone: string;
}
