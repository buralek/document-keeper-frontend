export class FilteredSalesChecksRequest {
  id: string;
  name: string;
  typeId: string;
  companyName: string;
  fromDate: Date;
  toDate: Date;
  userName: string;
  checkImages: string[];
}
