import { TestBed } from '@angular/core/testing';

import { SalesCheckFilterService } from './sales-check-filter.service';

describe('SalesCheckFilterService', () => {
  let service: SalesCheckFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesCheckFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
