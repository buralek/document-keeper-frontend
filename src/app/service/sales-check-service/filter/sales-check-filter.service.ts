import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FilteredSalesChecksResponse} from './model/filtered-sales-checks-response';
import {FilteredSalesChecksRequest} from './model/filtered-sales-checks-request';
import {FILTER_PATH, SALES_CHECK_SERVICE_PATH} from '../../../const/sales-check-service-const';

@Injectable({
  providedIn: 'root'
})
export class SalesCheckFilterService {

  constructor(private http: HttpClient) {
  }

  getFilteredSalesChecks(filteredSalesCheckRequest: FilteredSalesChecksRequest): Observable<FilteredSalesChecksResponse> {
    return this.http.post<FilteredSalesChecksResponse>(SALES_CHECK_SERVICE_PATH + FILTER_PATH, filteredSalesCheckRequest);
  }
}
