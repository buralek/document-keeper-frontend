export class GetSalesChecksByIdsRequest {
  salesCheckIds: string[];
  userName: string;
}
