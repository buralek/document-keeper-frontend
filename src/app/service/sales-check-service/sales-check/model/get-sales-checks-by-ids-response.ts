import {SalesCheckDto} from '../../model/sales-check-dto';

export class GetSalesChecksByIdsResponse {
  salesChecks: SalesCheckDto[];
}
