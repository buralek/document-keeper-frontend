import {WarrantyDto} from './warranty-dto';

export class AddSalesCheckRequest {
  name: string;
  typeId: string;
  companyId: string;
  userName: string;
  purchaseDate: Date;
  warrantyDto: WarrantyDto;
}
