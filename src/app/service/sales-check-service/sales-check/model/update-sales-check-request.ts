import {UpdateSalesCheckDto} from './update-sales-check-dto';

export class UpdateSalesCheckRequest {
  id: string;
  userName: string;
  updatePart: UpdateSalesCheckDto;
}
