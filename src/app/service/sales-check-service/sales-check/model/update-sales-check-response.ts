import {SalesCheckDto} from '../../model/sales-check-dto';

export class UpdateSalesCheckResponse {
  salesCheck: SalesCheckDto;
}
