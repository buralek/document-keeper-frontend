export class WarrantyDto {
  id: string;
  fromDate: Date;
  toDate: Date;
  warrantyImages: string[];
}
