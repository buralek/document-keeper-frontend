import {WarrantyDto} from './warranty-dto';

export class UpdateSalesCheckDto {
  name: string;
  typeId: string;
  purchaseDate: Date;
  companyId: string;
  warranty: WarrantyDto;
}
