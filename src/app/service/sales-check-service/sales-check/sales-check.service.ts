import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  ADD_PATH, DELETE_PATH,
  GET_SALES_CHECKS_BY_IDS_PATH,
  SALES_CHECK_PATH,
  SALES_CHECK_SERVICE_PATH,
  UPDATE_PATH
} from '../../../const/sales-check-service-const';
import {GetSalesChecksByIdsRequest} from './model/get-sales-checks-by-ids-request';
import {GetSalesChecksByIdsResponse} from './model/get-sales-checks-by-ids-response';
import {UpdateSalesCheckRequest} from './model/update-sales-check-request';
import {UpdateSalesCheckResponse} from './model/update-sales-check-response';
import {AddSalesCheckRequest} from './model/add-sales-check-request';
import {AddSalesCheckResponse} from './model/add-sales-check-response';
import {CookieService} from 'ngx-cookie-service';
import {USER_NAME} from '../../../const/auth-service-const';

@Injectable({
  providedIn: 'root'
})
export class SalesCheckService {

  constructor(private http: HttpClient,
              private cookieService: CookieService) { }

  getSalesChecksByIds(request: GetSalesChecksByIdsRequest): Observable<GetSalesChecksByIdsResponse> {
    return this.http.post<GetSalesChecksByIdsResponse>(SALES_CHECK_SERVICE_PATH + SALES_CHECK_PATH + GET_SALES_CHECKS_BY_IDS_PATH, request);
  }

  updateSalesCheck(request: UpdateSalesCheckRequest): Observable<UpdateSalesCheckResponse> {
    return this.http.put<UpdateSalesCheckResponse>(SALES_CHECK_SERVICE_PATH + SALES_CHECK_PATH + UPDATE_PATH, request);
  }

  addSalesCheck(request: AddSalesCheckRequest): Observable<AddSalesCheckResponse> {
    return this.http.post<AddSalesCheckResponse>(SALES_CHECK_SERVICE_PATH + SALES_CHECK_PATH + ADD_PATH, request);
  }

  deleteSalesCheck(salesCheckId: string): Observable<any> {
    const params = new URLSearchParams();
    params.append('salesCheckId', salesCheckId);
    params.append('userName', this.cookieService.get(USER_NAME));
    return this.http.delete(SALES_CHECK_SERVICE_PATH + SALES_CHECK_PATH + DELETE_PATH + '?' + params);
  }
}
