import { TestBed } from '@angular/core/testing';

import { SalesCheckService } from './sales-check.service';

describe('SalesCheckService', () => {
  let service: SalesCheckService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesCheckService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
