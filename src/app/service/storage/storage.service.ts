import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private filteredSalesCheckIds = new Subject<string[]>();
  public filteredSalesCheckIds$ = this.filteredSalesCheckIds.asObservable();

  constructor() { }

  changeFilteredSalesCheckIds(newSalesCheckIds: string[]) {
    this.filteredSalesCheckIds.next(newSalesCheckIds);
  }
}
