import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {ACCESS_TOKEN_COOKIE_KEY, GET_AUTH_CODE_PATH, GET_TOKEN_PATH, LOGIN_PATH, USER_NAME} from '../../../const/auth-service-const';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  static readonly CLIENT_ID = 'client-service';
  static readonly CLIENT_SECRET = 'clientServicePassword';
  static readonly REDIRECT_URI = 'http://localhost:4200/';
  static readonly SCOPE = 'client';
  static readonly GRANT_TYPE = 'authorization_code';
  static readonly RESPONSE_TYPE = 'code';
  private authenticated = false;

  constructor(private http: HttpClient,
              private cookieService: CookieService) {
  }

  login(login: string, password: string): Observable<string> {
    const loginParams = new URLSearchParams();
    loginParams.append('login', login);
    loginParams.append('password', password);

    return this.http.post<string>(LOGIN_PATH + '?' + loginParams, null).pipe();
  }

  getAuthCode(): Observable<string> {
    const params = new URLSearchParams();
    params.append('grant_type', AuthService.GRANT_TYPE);
    params.append('scope', AuthService.SCOPE);
    params.append('response_type', AuthService.RESPONSE_TYPE);
    params.append('client_id', AuthService.CLIENT_ID);

    return this.http.get(GET_AUTH_CODE_PATH + '?' + params, {withCredentials: true, responseType: 'text'}).pipe();
  }

  confirmAuthorize(): Observable<any> {
    const params = new URLSearchParams();
    params.append('user_oauth_approval', 'true');
    params.append('authorize', 'Authorize');
    return this.http.post(GET_AUTH_CODE_PATH + '?' + params, null, {withCredentials: true});
  }

  getTokenForCode(code): Observable<any> {
    const params = new URLSearchParams();
    params.append('grant_type', AuthService.GRANT_TYPE);
    params.append('code', code);
    params.append('scope', AuthService.SCOPE);
    const headers = new HttpHeaders({
      Authorization: 'Basic ' +  btoa(AuthService.CLIENT_ID + ':' + AuthService.CLIENT_SECRET),
      'Content-type': 'application/x-www-form-urlencoded'});

    return this.http.post(GET_TOKEN_PATH + '?' + params, null, {headers});
  }

  saveAuthInfo(token) {
    const expireDate = new Date().getTime() + (1000 * token.expires_in);
    this.cookieService.set(ACCESS_TOKEN_COOKIE_KEY, token.access_token, expireDate);
    this.cookieService.set(USER_NAME, token.userName, expireDate);
    console.log('Obtained Access token: ' + token);
    window.location.href = AuthService.REDIRECT_URI;
  }

  deleteToken() {
    this.cookieService.delete(ACCESS_TOKEN_COOKIE_KEY);
  }

  checkCredentials(): boolean {
    return this.cookieService.check(ACCESS_TOKEN_COOKIE_KEY);
  }

  getToken(): string {
    return this.cookieService.get(ACCESS_TOKEN_COOKIE_KEY);
  }

  setAuthenticated(authenticated: boolean) {
    this.authenticated = authenticated;
  }

  isAuthenticated() {
    return this.authenticated;
  }


}
