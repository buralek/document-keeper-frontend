export interface Account {
  id: string;
  firstName: string;
  middleName: string;
  secondName: string;
  createdDate: string;
  email: string;
}
