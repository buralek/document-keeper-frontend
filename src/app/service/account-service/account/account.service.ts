import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private findAllAccountsPath = 'http://localhost:3100/account-service/account/all';
  constructor(private http: HttpClient,
              private cookieService: CookieService) {
  }

  findAllAccounts1() {
    return this.http.get(this.findAllAccountsPath);
  }

  findAllAccounts(): Observable<any> {
    return this.http.get(this.findAllAccountsPath);
  }
}
