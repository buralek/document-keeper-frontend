import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {AuthService} from '../service/auth-service/auth/auth.service';
import {Router} from '@angular/router';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService,
              private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) { // client-side error
            errorMessage = `Client error: ${error.error.message}`;
          } else { // server-side error
            errorMessage = `Server error\nCode: ${error.status}\nMessage: ${error.message}`;
          }
          errorMessage = this.catch401Error(error, errorMessage);
          if (this.catchAuthCodeError(error)) {
            return new Observable<HttpEvent<any>>();
          }
          window.alert(errorMessage);
          return throwError(errorMessage);
        })
      );
  }

  private catchAuthCodeError(error): boolean {
    let code = '';
    if (error.url.includes('http://localhost:4000/redirect')) {
      const codeIndex = error.url.indexOf('code');
      code = error.url.substring(codeIndex + 5);
    }
    if (code !== '') {
      this.authService.getTokenForCode(code).subscribe(token => {
        this.authService.saveAuthInfo(token);
      });
      return true;
    } else {
      return false;
    }
  }

  private catch401Error(error, errorMessage: string): string {
    if (error.status === 401) {
      this.router.navigate(['login']);
      errorMessage = '401 Unauthorized\nPlease login';
    }
    return errorMessage;
  }
}
