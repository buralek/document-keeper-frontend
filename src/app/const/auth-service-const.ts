export const AUTH_SERVICE_PATH = 'http://localhost:4000/auth-service';

// Paths
export const GET_TOKEN_PATH = AUTH_SERVICE_PATH + '/oauth/token';
export const GET_AUTH_CODE_PATH = AUTH_SERVICE_PATH + '/oauth/authorize';
export const LOGIN_PATH = AUTH_SERVICE_PATH + '/login';

// Others
export const ACCESS_TOKEN_COOKIE_KEY = 'access_token';
export const USER_NAME = 'user_name';
