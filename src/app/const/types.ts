export enum Types {
  clothes = '2bf73919-b664-4438-8540-23a027bfaf9d',
  equipment = 'bd9851ad-71ec-42bc-9993-6588cb4030e0',
  plumbing = 'b0f8e5fe-f8c2-48ca-ba75-bf4a056fbc2b',
  furniture = '1d0a74fb-1794-4729-843b-93b86b6ad16c',
  car = 'cee88155-ec68-4569-8a8d-8ed71256aafb',
  hobby = 'e54eea7d-297f-469c-95b7-201c352c8f53'
}
