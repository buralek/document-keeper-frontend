export const SALES_CHECK_SERVICE_PATH = 'http://localhost:4000/sales-check-service';

// Paths
export const FILTER_PATH = '/filter';
export const SALES_CHECK_PATH = '/sales-check';
export const GET_SALES_CHECKS_BY_IDS_PATH = '/get-by-ids';
export const UPDATE_PATH = '/update';
export const COMPANY_PATH = '/company';
export const ALL_PATH = '/all';
export const ADD_PATH = '/add';
export const DELETE_PATH = '/delete';


