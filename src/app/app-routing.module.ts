import {NgModule} from '@angular/core';
import {TestComponent} from './component/test/test.component';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './component/authorization/login/login.component';
import {MainPageComponent} from './component/main-page/main-page.component';
import {CreateSalesCheckComponent} from './component/sales-check/create-sales-check/create-sales-check.component';

const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'login', component: LoginComponent},
  { path: 'test', component: TestComponent },
  { path: 'main', component: MainPageComponent },
  { path: 'create-sales-check', component: CreateSalesCheckComponent},
  { path: '**', redirectTo: ''}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
