# base image
FROM node:12.16.2

# set working directory
RUN mkdir -p /app
WORKDIR /app

COPY package.json /app/

RUN npm install
RUN npm install -g @angular/cli

COPY . /app

EXPOSE 4200

# start app
CMD ng serve --host 0.0.0.0


